import sys
import cv2
import numpy as np
from scipy.signal import fftconvolve
from matplotlib import pyplot as plt
import skimage.feature


def main(argv):
    image = cv2.imread('szop.jpg', 0)
    template = cv2.imread('pattern3.jpg', 0)
    rotated_pattern = np.rot90(np.rot90(template))

    if image.ndim < template.ndim:
        raise ValueError("Dimensionality of template must be less than or "
                         "equal to the dimensionality of image.")
    if np.any(np.less(image.shape, template.shape)):
        raise ValueError("Image must be larger than template.")

    convolution = fftconvolve(image, rotated_pattern, mode="same")

    coordinates = skimage.feature.peak_local_max(convolution, min_distance=20)

    best_co = find_best_peak(coordinates, image, template)

    plt.subplot(121), plt.imshow(template, cmap='gray')
    plt.title('Pattern'), plt.xticks([]), plt.yticks([])

    plt.subplot(122), plt.imshow(image, cmap='gray')
    plt.title('Peak local max'), plt.xticks([]), plt.yticks([])
    plt.plot(coordinates[:, 1], coordinates[:, 0], 'g.')
    plt.plot(best_co[1], best_co[0], 'r.')

    plt.show()

    # best_co = nahamov_method(template, image)
    # plt.subplot(121), plt.imshow(template, cmap='gray')
    # plt.title('Pattern'), plt.xticks([]), plt.yticks([])
    #
    # plt.subplot(122), plt.imshow(image, cmap='gray')
    # plt.title('Peak local max'), plt.xticks([]), plt.yticks([])
    # plt.plot(best_co[1], best_co[0], 'r.')
    #
    # plt.show()


def nahamov_method(template, image):
    min_sum = None
    best_co = [round(float(template.shape[0] / 2)), round(float(template.shape[1] / 2))]
    for x in range(round(float(template.shape[0] / 2)), image.shape[0]-round(float(template.shape[0] / 2))):
        for y in range(round(float(template.shape[1] / 2)), image.shape[1]-round(float(template.shape[1] / 2))):
            sum_of_diferences = 0
            x_start = x - round(float(template.shape[0] / 2))
            y_start = y - round(float(template.shape[1] / 2))
            for xx in range(0, template.shape[0]):
                for yy in range(0, template.shape[1]):
                    sum_of_diferences += abs(int(template[xx][yy]) - int(image[xx + x_start][yy + y_start]))
            if min_sum is None:
                min_sum = sum_of_diferences
                best_co = [x, y]
            if min_sum > sum_of_diferences:
                min_sum = sum_of_diferences
                best_co = [x, y]
    return best_co


def find_best_peak(coordinates, image, template):
    min_sum = None
    best_co = coordinates[0]
    for [x, y] in coordinates:
        sum_of_diferences = 0
        x_start = x - round(float(template.shape[0] / 2))
        y_start = y - round(float(template.shape[1] / 2))
        for xx in range(0, template.shape[0]):
            for yy in range(0, template.shape[1]):
                sum_of_diferences += abs(int(template[xx][yy]) - int(image[xx + x_start][yy + y_start]))
        if min_sum is None:
            min_sum = sum_of_diferences
            best_co = [x, y]
        if min_sum > sum_of_diferences:
            min_sum = sum_of_diferences
            best_co = [x, y]
    return best_co


if __name__ == "__main__":
    main(sys.argv)
