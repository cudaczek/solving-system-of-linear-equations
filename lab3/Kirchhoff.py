import sys
import argparse
import json
import networkx as nx
import matplotlib.pyplot as plt
import numpy

epsilon = 1e-7


#############################################


def print_graph(graph, path, num_of_figure, pos):
    plt.figure(num_of_figure)
    edge_labels = dict([((u, v,), d)
                        for u, v, d in graph.edges(data=True)])
    nx.draw(graph, pos, with_labels=True)
    nx.draw_networkx_edge_labels(graph, pos, edge_labels=edge_labels)
    nx.draw_networkx_edges(graph, pos, label='legenda')
    plt.savefig(path)
    return


########################################################


def compute_I_Kirchhoff_law(di_graph, matrix, y, num):
    edges_of_digraph = list(di_graph.edges)
    # print(edges_of_digraph)
    for node in di_graph.nodes():
        for a in di_graph.neighbors(node):
            edge = (node, a)
            j = edges_of_digraph.index(edge)
            matrix[num][j] += 1
        for b in di_graph.predecessors(node):
            edge = (b, node)
            j = edges_of_digraph.index(edge)
            matrix[num][j] -= 1
        num += 1
    return matrix, y


def compute_II_Kirchhoff_law(di_graph, graph):
    edges_of_digraph = list(di_graph.edges)
    nodes_of_digraph = list(di_graph.nodes)
    # print(edges_of_digraph)
    size = len(nodes_of_digraph) + len(nx.cycle_basis(graph))
    columns = len(edges_of_digraph)
    matrix = [[0] * columns for _ in range(size)]
    result_y = [0] * size
    # print(numpy.matrix(matrix))
    num = 0
    for ite, cycle in enumerate(nx.cycle_basis(graph)):
        # print(cycle)
        for i in range(len(cycle)):
            edge = (cycle[i], cycle[(i + 1) % len(cycle)])
            if edge in edges_of_digraph:
                j = edges_of_digraph.index(edge)
                x, y = edge
                keys = di_graph.edges[(x, y)].keys()
                if 'resistance' in keys:
                    matrix[ite][j] -= di_graph[x][y]['resistance']
                if 'emf' in keys:
                    result_y[ite] -= di_graph[x][y]['emf']
                if 'r' in keys:
                    matrix[ite][j] -= di_graph[x][y]['r']
            else:
                edge = cycle[(i + 1) % len(cycle)], cycle[i]
                x, y = edge
                if edge not in edges_of_digraph:
                    print("Wrong edge!")
                    raise Exception['Crazy graph']
                j = edges_of_digraph.index(edge)
                keys = di_graph.edges[(x, y)].keys()
                if 'resistance' in keys:
                    matrix[ite][j] += di_graph[x][y]['resistance']
                if 'emf' in keys:
                    result_y[ite] += di_graph[x][y]['emf']
                if 'r' in keys:
                    matrix[ite][j] += di_graph[x][y]['r']
        num += 1
    return matrix, result_y, num #, columns, size


###############################################

def create_di_graph(read_graph, read_emf, read_r):
    di_graph = nx.DiGraph()
    for node in read_graph:
        di_graph.add_edge(node['start'], node['end'], resistance=node['r'], emf=0, r=0)
    for node in read_emf:
        di_graph[node['start']][node['end']]['emf'] += node['EMF']
    for node in read_r:
        di_graph[node['start']][node['end']]['r'] += node['R']
    return di_graph


def create_graph(read_graph):
    graph = nx.Graph()
    for node in read_graph:
        graph.add_edge(node['start'], node['end'])
    return graph


def create_graf_from(edges, weights):
    graph = nx.DiGraph()
    for i, (u, v) in enumerate(edges):
        graph.add_edge(u, v, current=weights[i])
    return graph


###################################################################################################

def find_pivot(matrix, y, index):
    """
    :param matrix: 2D
    :param y: array
    :param index: currently computed column
    :return: matrix with changed rows
    """
    max_row = matrix[index]
    # print(max_row)
    max_row_index = index
    for i in range(index + 1, len(y)):
        # print(matrix.item(i, index), max_row[index])
        if abs(matrix.item((i, index))) > abs(max_row[index]):
            max_row = matrix[i]
            max_row_index = i
    if max_row_index != index:
        t = matrix[index].copy()
        z = matrix[max_row_index].copy()
        matrix[index] = z
        matrix[max_row_index] = t
    y[index], y[max_row_index] = y[max_row_index], y[index]
    return matrix, y


def eliminate(matrix, y, i):
    """
    :param matrix: 2D
    :param y: array
    :param i: currently computation starts with matrix[i][i]
    :return: Gaussian elimination of single column
    """
    size = len(matrix[0])
    for r in range(i + 1, len(y)):
        value = matrix.item((i, i))
        first_in_row = matrix.item((r, i))
        if abs(first_in_row) > epsilon:
            for c in range(i, size):
                matrix.itemset((r, c), (matrix.item((r, c)) * value / first_in_row) - matrix.item((i, c)))
            y[r] = y[r] * value / first_in_row - y[i]


def find_x(matrix, y):
    """
    :param matrix: 2D
    :param y: array
    k: num of linearly independent eqations - k = len(matrix[0])
    :return: array x result of: matrix * x = y

    """
    k = len(matrix[0])
    result = k * [0]
    for i in range(k):
        index = k - i - 1
        # print("len y ", len(y) - 1, "i", i, "k", k, " = index", index, " aa size ", size)
        result[index] = y[index]
        for j in range(k - 1, index, -1):
            # print(str(result[index]) + "=" + str(result[index]) + "-" + str(matrix.item((index, j))) + "*" + str(
            #     result[j]))
            result[index] = result[index] - matrix.item((index, j)) * result[j]
        # print(result)
        if abs(matrix.item((index, index))) > epsilon:
            # print(matrix.item((index, index)))
            result[index] = result[index] / matrix.item((index, index))
        else:
            if result[index] < epsilon:
                result[index] = "inf"
                # print(result[index])
                return "???"
            else:
                # print("Heh")
                return "No solution"
    for i in range(k, len(y)):
        sum = 0.0
        for j in range(k):
            # print(str(sum) + "+=" + str(matrix.item(i, j)) + "*" + str(result[j]))
            sum += matrix.item(i, j) * result[j]
        # print(sum, y[i], "eee?")
        if abs(sum - y[i]) > epsilon:
            # print("Ups")
            return "No solution"
    return result


def check_number_of_linear_dependent(matrix, y):
    first_zero_index = len(y)
    all_zeros = True
    k = 0
    i = len(y) - 1
    while all_zeros and i >= 0:
        if abs(y[i]) < epsilon:
            # print("sprawdzam ", i, y[i])
            for j in range(len(matrix[0]) - 1):
                if abs(matrix.item(i, j)) > epsilon:
                    all_zeros = False
                    # print(matrix.item(i, j))
        else:
            all_zeros = False
        if all_zeros:
            k += 1
            first_zero_index -= 1
            if i != first_zero_index:
                t = matrix[i].copy()
                z = matrix[first_zero_index].copy()
                matrix[i] = z
                matrix[first_zero_index] = t
                y[i], y[first_zero_index] = y[first_zero_index], y[i]
        all_zeros = True
        i -= 1
    # print("CHECKED")
    # print(matrix)
    # print(y)
    return len(y) - k


def count_result_using_pivot(matrix, y):
    """
    :param matrix: 2D - do not have to be square matrix
    :param y: array
    :return: Gaussian elimination with pivoting
    """
    for i in range(len(matrix[0])):
        matrix, y = find_pivot(matrix, y, i)
        eliminate(matrix, y, i)
    k = check_number_of_linear_dependent(matrix, y)
    # print(matrix)
    # print(y)
    if k == len(y):
        print("All are linearly independent")
        return "No solution"
    elif k == len(matrix[0]) + 1:
        print("One more linearly independent equations than unknows")
        return "No solution"
    elif k == len(matrix[0]):
        print("COUNTING")
        return find_x(matrix, y)
    else:  # k < len(matrix[0])
        print("Not enough linearly independent equations")
        return "Infinitely many solutions or no solutions"


#####################################################################################################################

def parse_input(args):
    try:
        with open(args.input) as file:
            reader = json.load(file)
    except TypeError:
        print("Wrong input!")

    if 'Connections' not in reader or "EMF" not in reader or "R" not in reader:
        print("Missing data in JSON file.")
        raise Exception('Missing data!')
    return reader['Connections'], reader['EMF'], reader['R']


def generate_graph(graph):
    di_graph = nx.DiGraph()
    di_graph.add_edges_from(graph.edges)
    for edge in di_graph.edges:
        di_graph.edges[edge]['resistance'] = numpy.random.uniform(-5, 5)
        if numpy.random.uniform(-5, 5) > 0.0:
            di_graph.edges[edge]['emf'] = numpy.random.uniform(-5, 5)
            di_graph.edges[edge]['r'] = numpy.random.uniform(-5, 5)
    return di_graph


####################################################################################################

def main(argv):
    """
    input format from json file
    for example see:
    {
  "Connections": [
    {"start": 0, "end": 1, "r": 0.5},
    {"start": 2, "end": 1, "r": 1},
    {"start": 2, "end": 3, "r": 0.6},
    {"start": 3, "end": 0, "r": 0},
    {"start": 3, "end": 1, "r": 1}

  ],
  "EMF": [
    {"start": 3, "end": 0, "EMF": 1.5}

  ],
  "R": [
    {"start": 3, "end": 1, "R": 1.2}
  ]
}
    """
    parser = argparse.ArgumentParser(description='Let\'s play with Kirchhoff...')
    parser.add_argument('-i', '--input', required=False, type=str, help='json input file ex. sample.json', )
    parser.add_argument('-o', '--output', required=False, help='output name file where graphs will be saved')
    args = parser.parse_args()
    if args.input is not None:
        read_graph, read_EMF, read_R = parse_input(args)
        di_graph = create_di_graph(read_graph, read_EMF, read_R)
        graph = create_graph(read_graph)
    else:
        # generate graph
        rand = numpy.random.uniform(-5, 5)
        if rand > 0.0:
            print("Complete graph generated")
            graph = nx.complete_graph(7)
            di_graph = generate_graph(graph)
        else:
            rand = numpy.random.uniform(-5, 5)
            if rand > 0.0:
                print("Complete bipartite graph generated")
                graph = nx.complete_bipartite_graph(3, 3)
                di_graph = generate_graph(graph)
            else:
                rand = numpy.random.uniform(-5, 5)
                if rand > 0.0:
                    print("Wheel graph generated")
                    graph = nx.wheel_graph(7)
                    di_graph = generate_graph(graph)
                else:
                    print("Cycle graph generated")
                    graph = nx.cycle_graph(10)
                    di_graph = generate_graph(graph)
    input_path = "input_graph.png"
    output_path = "output_graph.png"
    if args.output is not None:
        input_path = "in_" + args.output
        output_path = "out_" + args.output
    pos = nx.spring_layout(graph)
    print_graph(di_graph, input_path, 1, pos)
    matrix, y, num = compute_II_Kirchhoff_law(di_graph, graph)
    matrix, y = compute_I_Kirchhoff_law(di_graph, matrix, y, num)
    matrix = numpy.array(matrix)
    y = numpy.array(y)
    print(matrix)
    print(y)
    print("Final result: ")
    final_results = count_result_using_pivot(matrix, y)
    print(numpy.array(final_results))
    if final_results == "Infinitely many solutions or no solutions" or final_results == "No solution":
        final_results = ["?"] * len(matrix[0])
    final_graph = create_graf_from(di_graph.edges, final_results)
    print_graph(final_graph, output_path, 2, pos)
    plt.show()
    # print_graph(graph, "path.png")


if __name__ == "__main__":
    main(sys.argv)
